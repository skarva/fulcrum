/*
* Copyright (c) 2019 skarva LLC. <https://skarva.tech>
*
* This program is free software; you can redistribute it and/or
* modify it under the terms of the GNU General Public
* License as published by the Free Software Foundation; either
* version 3 of the License, or (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
* General Public License for more details.
*
* You should have received a copy of the GNU General Public
* License along with this program; if not, write to the
* Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
* Boston, MA 02110-1301 USA
*/

namespace Fulcrum.Widgets {
    public class WelcomeScreen : Granite.Widgets.Welcome {
        public signal void create_repo ();
        public signal void clone_repo ();
        public signal void open_repo ();

        public WelcomeScreen () {
            Object(
                title: _("Take Control"),
                subtitle: _("Create or open a git repository")
            );
        }

        construct {
            append ("list-add-symbolic", _("Create Repository"), _("Create a new git repository"));
            append ("insert-object-symbolic", _("Clone Repository"), _("Clone a remote git repository"));
            append ("folder-open-symbolic", _("Open Repository"), _("Open an existing local git repository"));

            valign = Gtk.Align.FILL;
            halign = Gtk.Align.FILL;
            vexpand = true;

            activated.connect ((index) => {
                switch (index) {
                    case 0:
                        create_repo ();
                        break;
                    case 1:
                        clone_repo ();
                        break;
                    case 2:
                        open_repo ();
                        break;
                }
            });
        }
    }
} // Fulcrum.Widgets
