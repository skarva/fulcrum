/*
* Copyright (c) 2019 skarva LLC. <https://skarva.tech>
*
* This program is free software; you can redistribute it and/or
* modify it under the terms of the GNU General Public
* License as published by the Free Software Foundation; either
* version 3 of the License, or (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
* General Public License for more details.
*
* You should have received a copy of the GNU General Public
* License along with this program; if not, write to the
* Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
* Boston, MA 02110-1301 USA
*/

namespace Fulcrum.Widgets {
    public class HeaderBar : Gtk.HeaderBar {
        private Gtk.Button fetch_button;
        private Gtk.Button pull_button;
        private Gtk.Button push_button;
        private Widgets.BranchSelector branch_list;

        public signal void send_notification (string msg);

        public HeaderBar (MainWindow parent) {
            Object(
                title: _("Fulcrum")
            );
        }

        construct {
            show_close_button = true;

            fetch_button = new Gtk.Button.from_icon_name("view-refresh-symbolic", Gtk.IconSize.LARGE_TOOLBAR);
            fetch_button.tooltip_text = "Fetch";
            fetch_button.sensitive = false;
            fetch_button.clicked.connect ( () => {
                send_notification (_("Fetch complete"));
            });

            pull_button = new Gtk.Button.from_icon_name ("go-down-symbolic", Gtk.IconSize.LARGE_TOOLBAR);
            pull_button.tooltip_text = "Pull";
            pull_button.sensitive = false;
            pull_button.clicked.connect ( () => {
                send_notification (_("Pull action complete"));
            });

            push_button = new Gtk.Button.from_icon_name ("go-up-symbolic", Gtk.IconSize.LARGE_TOOLBAR);
            push_button.tooltip_text = "Push";
            push_button.sensitive = false;
            push_button.clicked.connect ( () => {
                send_notification (_("Push complete"));
            });

            branch_list = new Widgets.BranchSelector ();

            pack_start (fetch_button);
            pack_start (pull_button);
            pack_start (push_button);
            pack_end (branch_list);

            show_all ();
        }

        public void update () {
            if (Application.repo != null) {
                fetch_button.sensitive = true;
                pull_button.sensitive = true;
                push_button.sensitive = true;
                branch_list.update ();
            }
        }
    }
} // Fulcrum.Widgets
