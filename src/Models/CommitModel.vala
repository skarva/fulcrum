/*
* Copyright (c) 2019 skarva LLC. <https://skarva.tech>
*
* This program is free software; you can redistribute it and/or
* modify it under the terms of the GNU General Public
* License as published by the Free Software Foundation; either
* version 3 of the License, or (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
* General Public License for more details.
*
* You should have received a copy of the GNU General Public
* License along with this program; if not, write to the
* Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
* Boston, MA 02110-1301 USA
*/

namespace Fulcrum.Models {
    public class CommitModel : Object, ListModel {
        private List<Ggit.Commit> commits;

        public void add (Ggit.Commit commit) {
            commits.append (commit);

            items_changed (commits.length () - 1, 0, 1);
        }

        public Object? get_item (uint position) {
            if (position > commits.length ()) {
                return null;
            }

            return commits.nth_data (position);
        }

        public Type get_item_type () {
            return typeof (Ggit.Commit);
        }

        public uint get_n_items () {
            return commits.length ();
        }

        public Object? get_object (uint position) {
            if (position > commits.length ()) {
                return null;
            }

            return commits.nth_data (position);
        }
    }
} // Fulcrum.Models
