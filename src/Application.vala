/*
* Copyright (c) 2019 skarva LLC. <https://skarva.tech>
*
* This program is free software; you can redistribute it and/or
* modify it under the terms of the GNU General Public
* License as published by the Free Software Foundation; either
* version 3 of the License, or (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
* General Public License for more details.
*
* You should have received a copy of the GNU General Public
* License along with this program; if not, write to the
* Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
* Boston, MA 02110-1301 USA
*/

namespace Fulcrum {
    public class Application : Gtk.Application {
        public static GLib.Settings saved_state;
        public static Ggit.Repository? repo;

        static construct {
            saved_state = new GLib.Settings ("com.github.skarva.fulcrum");
        }

        public Application () {
            Object (
                application_id: "com.github.skarva.fulcrum",
                flags: ApplicationFlags.FLAGS_NONE
            );

            Ggit.init ();
            repo = null;
        }

        protected override void activate() {
            var window = new MainWindow (this);

            window.show_all ();
        }

        public static int main (string[] args) {
            var app = new Application ();
            return app.run (args);
        }
    }
}
