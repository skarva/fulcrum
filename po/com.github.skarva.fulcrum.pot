# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the com.github.skarva.fulcrum package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: com.github.skarva.fulcrum\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-09-04 00:58-0400\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=CHARSET\n"
"Content-Transfer-Encoding: 8bit\n"

#: src/MainWindow.vala:27 data/com.github.skarva.fulcrum.desktop.in:3
#: data/com.github.skarva.fulcrum.appdata.xml.in:6
msgid "Fulcrum"
msgstr ""

#: data/com.github.skarva.fulcrum.desktop.in:4
msgid "Git GUI"
msgstr ""

#: data/com.github.skarva.fulcrum.desktop.in:5
msgid "Manage git repositories for games"
msgstr ""

#: data/com.github.skarva.fulcrum.desktop.in:8
msgid "con.github.skarva.fulcrum"
msgstr ""

#: data/com.github.skarva.fulcrum.desktop.in:12
msgid "Git;Code;Version;Control;"
msgstr ""

#: data/com.github.skarva.fulcrum.appdata.xml.in:7
msgid "Leverage git for games"
msgstr ""

#: data/com.github.skarva.fulcrum.appdata.xml.in:9
msgid ""
"A git GUI for game developers. With the flip of a switch, change the "
"workflow to better suit your needs as a programmer or an artist."
msgstr ""
