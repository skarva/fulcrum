/*
* Copyright (c) 2019 skarva LLC. <https://skarva.tech>
*
* This program is free software; you can redistribute it and/or
* modify it under the terms of the GNU General Public
* License as published by the Free Software Foundation; either
* version 3 of the License, or (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
* General Public License for more details.
*
* You should have received a copy of the GNU General Public
* License along with this program; if not, write to the
* Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
* Boston, MA 02110-1301 USA
*/

namespace Fulcrum {
    public class MainWindow : Gtk.ApplicationWindow {
        public bool art_workflow { get; set; }

        private Gtk.Stack layout_stack;
        private Widgets.HeaderBar header_bar;
        private Widgets.WelcomeScreen welcome;
        private Views.TimelineView timeline_view;

        public MainWindow (Fulcrum.Application app) {
            Object (
                application: app
            );
        }

        construct {
            /* Load gsettings */
            set_default_size (1280, 720);
            window_position = Gtk.WindowPosition.CENTER;

            /* Init Layout */
            header_bar = new Widgets.HeaderBar (this);
            set_titlebar (header_bar);

            layout_stack = new Gtk.Stack ();
            layout_stack.transition_type = Gtk.StackTransitionType.SLIDE_LEFT_RIGHT;

            welcome = new Widgets.WelcomeScreen ();
            welcome.create_repo.connect (create_repo);
            welcome.clone_repo.connect (clone_repo);
            welcome.open_repo.connect (open_repo);

            add (welcome);

            show_all ();

            header_bar.send_notification.connect ( (msg) => {
                var notification = new Notification (_("Fulcrum"));
                notification.set_body (msg);
                application.send_notification ("com.github.skarva.fulcrum", notification);
            });
        }

        private void load_workspace () {
            if (Application.repo != null) {
                header_bar.update ();
                timeline_view = new Views.TimelineView ();
                layout_stack.add_named (timeline_view, "timeline");

                remove (welcome);
                add (layout_stack);

                show_all ();
            }
        }

        private void create_repo () {
            // Create repo dialog, save path to Application.saved_state
            var dialog = new Gtk.FileChooserNative (_("Create Repository"), this,
                            Gtk.FileChooserAction.SELECT_FOLDER, _("Create"), _("Cancel"));
            var result = dialog.run ();
            if (result == Gtk.ResponseType.ACCEPT) {
                var folder_uri = File.new_for_uri (dialog.get_current_folder_uri ());
                try {
                    Application.repo = Ggit.Repository.init_repository (folder_uri, false);
                    load_workspace ();
                } catch (Error e) {
                    var error_dialog = new Granite.MessageDialog.with_image_from_icon_name (
                        _("Error creating repository"), e.message);
                    error_dialog.run ();
                    error_dialog.destroy ();
                }
            }
            dialog.destroy ();
        }

        private void clone_repo () {
            // Clone repo dialog, save path to Application.saved_state
            var dialog = new Dialogs.CloneDialog (this);
            var result = dialog.run ();
            if (result == Gtk.ResponseType.ACCEPT) {
                var url = dialog.url_entry.text;
                var folder_uri = File.new_for_uri (dialog.uri_chooser.get_current_folder_uri ());
                try {
                    Application.repo = Ggit.Repository.clone (url, folder_uri, new Ggit.CloneOptions ());
                    load_workspace ();
                } catch (Error e) {
                    var error_dialog = new Granite.MessageDialog.with_image_from_icon_name (
                        _("Error cloning repository"), e.message);
                    error_dialog.run ();
                    error_dialog.destroy ();
                }
            }
            dialog.destroy ();
        }

        private void open_repo () {
            // Open repo dialog, save path to Application.saved_state
            var dialog = new Gtk.FileChooserNative (_("Open Repository"), this,
                            Gtk.FileChooserAction.SELECT_FOLDER, _("Open"), _("Cancel"));
            var result = dialog.run ();
            if (result == Gtk.ResponseType.ACCEPT) {
                var folder_uri = File.new_for_uri (dialog.get_current_folder_uri ());
                try {
                    Application.repo = Ggit.Repository.open (folder_uri);
                    load_workspace ();
                } catch (Error e) {
                    var error_dialog = new Granite.MessageDialog.with_image_from_icon_name (
                        _("Error opening repository"), e.message);
                    error_dialog.run ();
                    error_dialog.destroy ();
                }
            }
            dialog.destroy ();
        }
    }
}
