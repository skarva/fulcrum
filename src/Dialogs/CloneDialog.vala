/*
* Copyright (c) 2019 skarva LLC. <https://skarva.tech>
*
* This program is free software; you can redistribute it and/or
* modify it under the terms of the GNU General Public
* License as published by the Free Software Foundation; either
* version 3 of the License, or (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
* General Public License for more details.
*
* You should have received a copy of the GNU General Public
* License along with this program; if not, write to the
* Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
* Boston, MA 02110-1301 USA
*/

namespace Fulcrum.Dialogs {
    class CloneDialog : Gtk.Dialog {
        public Gtk.Entry url_entry { get; private set; }
        public Gtk.FileChooserButton uri_chooser { get; private set; }

        public CloneDialog(Gtk.Window? parent) {
            Object(
                border_width: 12,
                deletable: false,
                resizable: true,
                transient_for: parent,
                modal: true,
                title: _("Clone Repository")
            );

            set_default_response (Gtk.ResponseType.ACCEPT);
        }

        construct {
            var grid = new Gtk.Grid ();
            grid.column_spacing = 12;
            grid.row_spacing = 6;
            grid.margin_bottom = 24;
            get_content_area().add (grid);

            var header = new Granite.HeaderLabel (_("Clone Repository"));
            grid.attach (header, 0, 0, 2, 1);

            var url_label = new Gtk.Label (_("URL:"));
            url_label.halign = Gtk.Align.END;
            url_entry = new Gtk.Entry ();
            url_entry.hexpand = true;
            url_entry.activates_default = true;
            grid.attach (url_label, 0, 1, 1, 1);
            grid.attach (url_entry, 1, 1, 1, 1);

            var uri_label = new Gtk.Label (_("Clone To:"));
            uri_label.halign = Gtk.Align.END;
            uri_chooser = new Gtk.FileChooserButton (_("Select a location"), Gtk.FileChooserAction.SELECT_FOLDER);
            grid.attach (uri_label, 0, 2, 1, 1);
            grid.attach (uri_chooser, 1, 2, 1, 1);

            add_button (_("Cancel"), Gtk.ResponseType.CLOSE);
            var save = add_button (_("Clone"), Gtk.ResponseType.ACCEPT);
            save.get_style_context ().add_class (Gtk.STYLE_CLASS_SUGGESTED_ACTION);

            show_all ();
        }
    }
}
