/*
* Copyright (c) 2019 skarva LLC. <https://skarva.tech>
*
* This program is free software; you can redistribute it and/or
* modify it under the terms of the GNU General Public
* License as published by the Free Software Foundation; either
* version 3 of the License, or (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
* General Public License for more details.
*
* You should have received a copy of the GNU General Public
* License along with this program; if not, write to the
* Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
* Boston, MA 02110-1301 USA
*/

namespace Fulcrum.Views {
    public class TimelineView : Gtk.Paned {
        private Models.CommitModel history_model;
        private Models.WorkingCopyModel unstaged_model;
        private Models.WorkingCopyModel staged_model;
        private Gtk.ListBox history_list;
        private Gtk.ListBox unstaged_list;
        private Gtk.ListBox staged_list;

        construct {
            var layout = new Gtk.Box (Gtk.Orientation.HORIZONTAL, 0);
            add (layout);

            history_model = new Models.CommitModel ();
            history_list = new Gtk.ListBox ();
            history_list.bind_model (history_model, add_commit_row);

            unstaged_model = new Models.WorkingCopyModel ();
            unstaged_list = new Gtk.ListBox ();
            unstaged_list.bind_model (unstaged_model, add_item_row);

            staged_model = new Models.WorkingCopyModel ();
            staged_list = new Gtk.ListBox ();
            staged_list.bind_model (staged_model, add_item_row);

            update_lists ();

            layout.add (history_list);
            layout.add (unstaged_list);
            layout.add (staged_list);

            show_all ();
        }

        public void update_lists () {
            assert (Application.repo != null);

            try {
                var revision_walker = new Ggit.RevisionWalker (Application.repo);
                revision_walker.push_head ();
                Ggit.OId? revision_id = revision_walker.next ();
                while (revision_id != null) {
                    var commit = Application.repo.lookup_commit (revision_id);
                    history_model.add (commit);
                    revision_id = revision_walker.next ();
                }
                history_list.show_all ();
            } catch (Error e) {
                critical ("Cannot load commit history item: %s", e.message);
            }

            try {
                var options = new Ggit.StatusOptions (Ggit.StatusOption.DEFAULT, Ggit.StatusShow.INDEX_AND_WORKDIR, null);
                Application.repo.file_status_foreach (options, file_status);
                unstaged_list.show_all ();
                staged_list.show_all ();
            } catch (Error e) {
                critical ("Cannot load working file: %s", e.message);
            }
        }

        private Gtk.Widget add_commit_row (Object? item) {
            var commit = item as Ggit.Commit;
            var row = new Widgets.CommitListBoxRow (commit);

            return row;
        }

        private Gtk.Widget add_item_row (Object? item) {
            var git_item = item as Models.Item;
            var label = new Gtk.Label (git_item.path);
            var row = new Gtk.ListBoxRow ();
            row.add (label);

            return row;
        }

        private int file_status (string path, Ggit.StatusFlags status) {
            if (Ggit.StatusFlags.WORKING_TREE_NEW in status ||
                Ggit.StatusFlags.WORKING_TREE_MODIFIED in status ||
                Ggit.StatusFlags.WORKING_TREE_DELETED in status) {
                unstaged_model.add (path);
            } else if (Ggit.StatusFlags.INDEX_NEW in status ||
                       Ggit.StatusFlags.INDEX_MODIFIED in status ||
                       Ggit.StatusFlags.INDEX_DELETED in status ||
                       Ggit.StatusFlags.INDEX_RENAMED in status) {
                staged_model.add (path);
            }

            return 0;
        }
    }
} // Fulcrum.Views
