/*
* Copyright (c) 2019 skarva LLC. <https://skarva.tech>
*
* This program is free software; you can redistribute it and/or
* modify it under the terms of the GNU General Public
* License as published by the Free Software Foundation; either
* version 3 of the License, or (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
* General Public License for more details.
*
* You should have received a copy of the GNU General Public
* License along with this program; if not, write to the
* Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
* Boston, MA 02110-1301 USA
*/

namespace Fulcrum.Widgets {
    class BranchSelector : Gtk.ComboBoxText {
        construct {
            sensitive = false;
        }

        public void update () {
            if (Application.repo != null) {
                Ggit.Branch? current_branch = null;
                Ggit.BranchEnumerator? branches = null;

                try {
                    current_branch = Application.repo.get_head () as Ggit.Branch;
                    branches = Application.repo.enumerate_branches (Ggit.BranchType.LOCAL);
                } catch (Error e) {
                    critical ("Cannot create branch selector: %s", e.message);
                    sensitive = false;
                }

                if (current_branch == null || branches == null) {
                    sensitive = false;
                } else {
                    var id_counter = 0;
                    foreach (var branch_ref in branches) {
                        var branch = branch_ref as Ggit.Branch;
                        string? branch_name = null;
                        try {
                            branch_name = branch.get_name ();
                            if (branch_name != null) {
                                var ref_name = branch_ref.get_name ();
                                if (ref_name != null) {
                                    append (id_counter.to_string (), branch_name);

                                    if (branch_name == current_branch.get_name ()) {
                                        active_id = id_counter.to_string ();
                                    }

                                    id_counter++;
                                }
                            }
                        } catch (Error e) {
                            critical ("Failed to create entry for branch %s. %s", branch_name ?? "unknown", e.message);
                        }
                    }
                }

                sensitive = true;
            }

            show_all ();
        }
    }
} // Fulcrum.Widgets
