# Fulcrum

## Leverage git

A git GUI for game developers. With the flip of a switch, change the workflow to better suit your needs as a programmer or an artist.

## Developing and Building

You'll need the following dependencies:
* libgtk-3-dev
* meson
* valac

Run `meson build` to configure the build environment. Change directory to the build directory and run `ninja` to build

    meson build --prefix=/usr
    cd build
    ninja

To install, use `ninja install`, then execute with `com.github.skarva.fulcrum`

    sudo ninja install
    com.github.skarva.fulcrum
